# -*- coding: utf-8 -*-
import os
import FeatureExtractionUtilities
import nltk
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from KaggleWord2VecUtility import KaggleWord2VecUtility
import pandas as pd
from sklearn import svm
from collections import defaultdict
import numpy as np
import glob
import json
import re
from bs4 import BeautifulSoup 
from nltk.corpus import stopwords 
from nltk.stem import SnowballStemmer
from nltk.stem.porter import *
from sklearn import preprocessing
from nltk.corpus import brown


stemmer = PorterStemmer()
def preprocess(text):
    text = text.lower()
    return text

def token_freqs(doc):
    #Extract a dict mapping tokens from doc to their frequencies.
    freq = defaultdict(int)
    for tok in tokens(doc):
        freq[tok] += 1
    return freq

def tokens(doc):
   
    return (tok.lower() for tok in re.findall(r"\w+", doc))

def review_to_words( raw_review ):
    # Function to convert a raw review to a string of words
    # The input is a single string (a raw movie review), and 
    # the output is a single string (a preprocessed movie review)
    #
    # 1. Remove HTML
    #review_text = BeautifulSoup(raw_review).get_text()
    #
    # 2. Remove non-letters        
    #letters_only = re.sub("[^a-zA-Z]", " ", review_text)
    #
    # 3. Convert to lower case, split into individual words
    words = [stemmer.stem(w) for w in raw_review.lower().split()]
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    #stops = set(stopwords.words("english"))
    # 
    # 5. Remove stop words
    #meaningful_words = [w for w in words if not w in stops]
    #
    # 6. Join the words back into one string separated by space, 
    # and return the result.
    return( " ".join( words ))

def part_of_speech_tagging(raw_review):
    suffix_fdist = nltk.FreqDist()
    for word in raw_review:
        word = word.lower()
        suffix_fdist[word[-1:]] += 1
        suffix_fdist[word[-2:]] += 1
        suffix_fdist[word[-3:]] += 1
    common_suffixes = [suffix for (suffix, count) in suffix_fdist.most_common(100)]
    features = {}
    for word in raw_review:    
        for suffix in common_suffixes:
            features['endswith({})'.format(suffix)] = word.lower().endswith(suffix)
    return features 
    
    
if __name__ == '__main__':
    with open('train_data.json','rb') as f:
        data1 = f.readlines()
     
    data1 = map(lambda x: x.rstrip(), data1)
    data_json_str1 = "[" + ','.join(data1) + "]"
    data_df1 = pd.read_json(data_json_str1)
 
    with open('testing_data.json','rb') as g:
        data2 = g.readlines()
     
    data2 = map(lambda y: y.rstrip(), data2)
    data_json_str2 = "[" + ','.join(data2) + "]"
    data_df2 = pd.read_json(data_json_str2)
 

    #vectorizer = CountVectorizer(ngram_range=(1,3), analyzer = "word", tokenizer = None, preprocessor = None, stop_words = {'english'}, max_features = 5000)
    vectorizer = DictVectorizer()
    #vectorizer = TfidfVectorizer()
    #---------
    processed_data_df1 = data_df1["review"]
    processed_training_data = []
    for p in processed_data_df1:
        p = review_to_words(p)  
        processed_training_data.append(p)                              


    trained_data = vectorizer.fit_transform(token_freqs(a) for a in processed_training_data).toarray()
    #trained_data = trained_data.toarray()
    #trained_data = vectorizer.fit_transform(processed_training_data)

    train_structural_features = FeatureExtractionUtilities.getstructuralfeatures(processed_training_data)
    scaler = preprocessing.StandardScaler().fit(train_structural_features)
    train_structural_features = scaler.transform(train_structural_features)
    #max_len_train = max(train_structural_features[:][0])
    #print train_structural_features
    #max_num_sents = max(train_structural_features[:][-1])
    #max_ave_len = max(train_structural_features[:][1])

    #print max_len_train
    #min_max_scaler = preprocessing.MinMaxScaler()
    #features = min_max_scaler.fit_transform(features)

    trained_data = np.concatenate((trained_data,train_structural_features),axis=1)

    #identifying dialogue act types
    #train_structural_features = [(FeatureExtractionUtilities.dialogue_act_features(post), post.) for post in processed_training_data]
    
    #train_parts_of_speech_tagging = part_of_speech_tagging(b for b in processed_training_data)
    #scaler = preprocessing.StandardScaler().fit(train_parts_of_speech_tagging)
    #train_parts_of_speech_tagging = scaler.transform(train_parts_of_speech_tagging)
    
    #trained_data = np.concatenate((trained_data,train_parts_of_speech_tagging),axis=1)
    #recognizing textual entailment  
    #train_structural_features = [(nltk.RTEFeatureExtractor())  for post in processed_training_data]
    #scaler = preprocessing.StandardScaler().fit(train_structural_features)
    #train_structural_features = scaler.transform(train_structural_features)

    #trained_data = np.concatenate((trained_data,train_structural_features),axis=1)

    #print train_structural_features
    #print trained_data
    #feature ideas: n-grams (1-3), review length, average sentence lengths, number of sentences
              
    #forest = RandomForestClassifier(n_estimators=2000)
    forest = svm.SVC(C=128.0, cache_size=200, class_weight=None, coef0=0.0, degree=3,
              gamma=0.0, kernel='rbf', max_iter=-1, probability=True, random_state=None,
              shrinking=True, tol=0.001, verbose=False)
    forest = forest.fit( trained_data, data_df1["sentiment"] )

    #---------------------------

    processed_data_df2 = data_df2["review"]



    processed_testing_data = []
    for q in processed_data_df2:
        q = review_to_words(q)  
        processed_testing_data.append(q)                           
    testing_data = vectorizer.transform(token_freqs(a) for a in processed_testing_data)
    testing_data = testing_data.toarray()
    #testing_data = vectorizer.transform(processed_testing_data)
    test_structural_features = FeatureExtractionUtilities.getstructuralfeatures(processed_testing_data)
    test_structural_features = scaler.transform(test_structural_features)
    testing_data = np.concatenate((testing_data,test_structural_features),axis=1)

    #test = test.toarray()

    # Use the random forest to make sentiment label predictions
    result = forest.predict(testing_data)

    # Copy the results to a pandas dataframe with an "id" column and
    # a "sentiment" column
    output = pd.DataFrame( data={"id":data_df2["ID"], "sentiment_result":result, "annotated_sentiment":data_df2["sentiment"]} )

    # Use pandas to write the comma-separated output file
    #output.to_csv(os.path.join(os.path.dirname(__file__), 'data', 'Bag_of_Words_model.csv'), index=False, quoting=3)
    output.to_csv(os.path.join('./', '', 'Bag_of_Words_model.csv'), index=False, quoting=3)

    print "Wrote results to Bag_of_Words_model.csv"
    print accuracy_score(data_df2["sentiment"],result)        
              
      
         
         
     
     #Fout.close()
     #print len(train)

